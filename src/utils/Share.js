import Share from 'react-native-share';
import { Platform } from "react-native";
import { useMemo } from 'react';
export const share = (message, url) => {

  const shareOptions = Platform.select({
    default: {
      title:"share via",
      subject: message,
      message: `${message} ${url}`,
    },
  });
  
  Share.open(shareOptions)
    .then(res => {
      console.log(res);
    })
    .catch(err => {
      err && console.log(err);
    });
};
