import { combineReducers } from 'redux';

import network from './network';
import lang from './lang';

export default combineReducers({
  network,
  lang,
});
