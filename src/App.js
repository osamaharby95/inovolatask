import { Navigation } from 'react-native-navigation';
import regterScreens from './screens';
import { AppNavigation } from './common';
import appLaunchConfig from './utils/AppLaunchConfig';
import store from './store/store';
import { Linking, AsyncStorage } from 'react-native';

export default () => {
  //appLaunch
  Navigation.events().registerAppLaunchedListener(async () => {
    //default app config
    await appLaunchConfig();
    //navigation config
    AppNavigation.setNavigationDefaultOptions();

    //screens
    regterScreens();
    await appLaunchConfig();
    const rtl = store.getState().lang.rtl;
      AppNavigation.navigateToHome(rtl);
  });
};
