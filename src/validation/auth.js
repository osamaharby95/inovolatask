import * as yup from 'yup';
import I18n from 'react-native-i18n';
var nameRegex = /^[a-zA-Z\s]+$/;
export default class Auth {
  signIn = (validationSchema = values =>
    yup.object().shape({
      mobile: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .test('mobile', I18n.t('enter-valid-phone-number'), () =>
          Number.isInteger(+values.mobile) && !(values.mobile).includes(".")
          )        
        .min(5, `${I18n.t('must-be-atleast')} 5 ${I18n.t('numbers')}`)
        .max(15, `${I18n.t('must-be-atmost')} 15 ${I18n.t('numbers')}`)
        ,
      password: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .min(6, `${I18n.t('must-be-atleast')} 6 ${I18n.t('char')}`),
    }));
  signUp = (validationSchema = values =>
    yup.object().shape({
      mobile: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .test('mobile', I18n.t('enter-valid-phone-number'), () =>
        Number.isInteger(+values.mobile) && !(values.mobile).includes(".")
        )
        .min(5, `${I18n.t('must-be-atleast')} 5 ${I18n.t('numbers')}`)
        .max(15, `${I18n.t('must-be-atmost')} 15 ${I18n.t('numbers')}`)
      ,
      password: yup
        .string()
        .required(I18n.t('validation-error-required'))
    .min(6, `${I18n.t('must-be-atleast')} 6 ${I18n.t('char')}`),
      password_confirmation: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .oneOf([values.password, ''], I18n.t('signup-confirmPassword-invalid'))
        .min(6, `${I18n.t('must-be-atleast')} 6 ${I18n.t('char')}`),
      name: yup
        .string()
        .required(I18n.t('validation-error-required'))
        // .matches(nameRegex, `${I18n.t('invalid')}`)
        .min(3, `${I18n.t('must-be-atleast')} 3 ${I18n.t('char')}`)
        .max(25, `${I18n.t('must-be-atmost')} 25 ${I18n.t('char')}`),

      country_id: yup.string().required(I18n.t('validation-error-required')),
    }));
  forgetPassword = (validationSchema = values =>
    yup.object().shape({
      mobile: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .test('mobile', I18n.t('enter-valid-phone-number'), () =>
        Number.isInteger(+values.mobile) && !(values.mobile).includes(".")
        
        )
        .min(5, `${I18n.t('must-be-atleast')} 5 ${I18n.t('numbers')}`)
        .max(15, `${I18n.t('must-be-atmost')} 15 ${I18n.t('numbers')}`)
      ,
    }));
  confirmPassword = (validationSchema = values =>
    yup.object().shape({
      password: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .min(6, `${I18n.t('must-be-atleast')} 6 ${I18n.t('char')}`),
      password_confirmation: yup
        .string()
        .required(I18n.t('validation-error-required'))
        .oneOf([values.password, ''], I18n.t('signup-confirmPassword-invalid'))
        .min(6, `${I18n.t('must-be-atleast')} 6 ${I18n.t('char')}`),
    }));
}
