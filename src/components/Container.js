import React from 'react';
import {AppView} from '../common';
import Header from './Header';

const Container = props => {
  const {
    backHandler,
    header,
    hideBack,
    title,
    showMenu,
    showCart,
    bold,
    logo,
    search,
    children,
    rowItems,
    transparent,
    ...rest
  } = props;
  return (
    <AppView flex stretch>
      {header && (
        <Header 
          {...{search}}
          {...{logo}}
          {...{bold}}
          {...{backHandler}}
          {...{hideBack}}
          {...{showMenu}}
          {...{showCart}}
          {...{title}}
          {...{transparent}}
          {...{rowItems}}
        />
      )}
      <AppView flex stretch {...rest}>
        {children}
      </AppView>
    </AppView>
  );
};
Container.defaultProps = {
  header: true,
  bold: true,
};

export default Container;
