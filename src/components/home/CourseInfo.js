import React from 'react';
import { AppText, AppView, AppIcon, } from '../../common';
import moment from "moment"
import "moment/locale/ar";

const CourseInfo = ({ data }) => {
  return (
    <AppView  stretch padding={5} marginTop={3} borderBottomWidth={.7} borderBottomColor="#B7B6C6">
      <AppText  marginBottom={2}>
        # {data.interest}
      </AppText>
      <AppText color="#9E9EA6" bold size={8}  marginBottom={3}>
        {data.title}
      </AppText>
    <AppView row stretch marginBottom={3}>
      <AppIcon
        name="calendar-month-outline"
        type="material-community"         
      />
      <AppText marginHorizontal={5}>
        {moment(data.date).format("dddd, Do MMMM , h:mm a")}
      </AppText>
    </AppView>
    <AppView row stretch marginBottom={3}>
      <AppIcon
        name="pushpino"
        type="ant"         
        flip
      />
      <AppText marginHorizontal={5}>
        {data.address}
      </AppText>
    </AppView>

    </AppView>

  );
};

export default CourseInfo;
