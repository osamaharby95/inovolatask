import React from 'react';
import { AppText, AppView, } from '../../common';
const CourseCost = ({ data }) => {
  return (
    <AppView stretch padding={5} marginTop={3} marginBottom={10} >
      <AppText bold marginBottom={3}>
        تكلفة الدورة
      </AppText>
      {data.reservTypes.map((item, index) =>
        <AppView spaceBetween row stretch key={item.id} marginBottom={3}>
          <AppText >
            {item.name}
          </AppText>
          <AppText >
            SAR {item.price}
          </AppText>

        </AppView>
      )}
    </AppView>

  );
};

export default CourseCost;
