import React from 'react';
import { AppText, AppView, } from '../../common';
const AboutCourse = ({ data }) => {
  return (
    <AppView  stretch padding={5} marginTop={3} borderBottomWidth={.7} borderBottomColor="#B7B6C6">
      <AppText marginBottom={3} bold>
        عن الدورة
      </AppText>
      <AppText >
        {data.occasionDetail}
      </AppText>

    </AppView>

  );
};

export default AboutCourse;
