import React, {  useState } from 'react';
import {  AppView, AppIcon } from '../../common';
import CustomSwiper from '../../components/CustomSwiper';
import { Swiper } from '../../components';
import { share } from '../../utils/Share';
import { BackHandler } from 'react-native';

const CourseSwiper = ({ data }) => {
  const [fav, setFav] = useState(false)

  return (
    <AppView  stretch >
      <Swiper
        fromApi
        resizeMode="stretch"
        data={data.img}
        SwiperComponent={CustomSwiper}
      />
      <AppView stretch height={7} backgroundColor="transparent" row centerY spaceBetween
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          right: 0
        }}
      >
        <AppIcon
          flip
          name="ios-arrow-back"
          type="ion"
          size={10}
          marginHorizontal={8}
          color="#fff"
          onPress={BackHandler.exitApp}
        />

        <AppView stretch row flex bottom >
          <AppIcon
            flip
            name="sharealt"
            type="ant"
            size={10}
            color='#fff'
            onPress={() => share("welcome", "")}
            flip={false}
            marginHorizontal={5}
          />
          <AppIcon
            flip
            name="ios-star-outline"
            type="ion"
            size={10}
            color={fav ? 'primary' : "#fff"}
            onPress={() =>
              setFav(fav => !fav)
            }
            marginHorizontal={5}
          />
        </AppView>
      </AppView>
    </AppView>

  );
};

export default CourseSwiper;
