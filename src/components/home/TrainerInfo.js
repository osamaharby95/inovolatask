import React from 'react';
import { AppText, AppView, AppImage, } from '../../common';
import avatar from "../../assets/avatar.png" 
const TrainerInfo = ({ data }) => {
  return (
    <AppView  stretch padding={5} marginTop={3} borderBottomWidth={.7} borderBottomColor="#B7B6C6">
    <AppView row stretch marginBottom={3}>
      <AppImage
        // TODO there is a problem with the image from url
        // source={{uri:data.trainerImg}}
        source={avatar}
        circleRadius={10}
      />
      <AppText marginHorizontal={5} bold>
        {data.trainerName}
      </AppText>
    </AppView>
      <AppText >
        {data.trainerInfo}
      </AppText>

    </AppView>

  );
};

export default TrainerInfo;
