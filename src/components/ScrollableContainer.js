import React from 'react';
import {AppView, AppScrollView} from '../common';
import Header from './Header';

const ScrollableContainer = props => {
  const {
    backHandler,
    header,
    hideBack,
    title,
    showMenu,
    showCart,
    bold,
    logo,
    search,
    children,
    transparent,
    rowItems,
    ...rest
  } = props;
  return (
    <AppView flex stretch>
      {header && (
        <Header
          {...{logo}}
          {...{search}}
          {...{bold}}
          {...{backHandler}}
          {...{hideBack}}
          {...{showMenu}}
          {...{showCart}}
          {...{title}}
          {...{transparent}}
          {...{rowItems}}
        />
      )}
      <AppScrollView {...rest} flex stretch>
        {children}
      </AppScrollView>
    </AppView>
  );
};

ScrollableContainer.defaultProps = {
  header: true,
  bold: true,
};

export default ScrollableContainer;
