import React from 'react';

import {
  AppView,
  AppText,
  AppNavigation,
  AppButton,
  AppIcon,
  TouchableView,
} from '../common';
import {APPBAR_HEIGHT} from '../common/utils/responsiveDimensions';
const goTo = name => {
  AppNavigation.push(name);
};
const Header = props => {
  const {
    backHandler,
    hideBack,
    title,
    showMenu,
    logo,
    bold,
    transparent,
    rowItems,
  } = props;
  const goBack = () => {
    if (backHandler) {
      backHandler();
    } else {
      AppNavigation.pop();
    }
  };

  const renderRight = () => {
    if (rowItems && rowItems.length > 0) {
      return (
        <AppView row flex={2} stretch>
          {rowItems.map(item =>
            React.cloneElement(item, {
              key: String(Math.random()),
            }),
          )}
        </AppView>
      );
    }
    return <AppView stretch flex />;
  };

  const renderLeft = () => {
    if (hideBack) {
      return <AppView stretch flex />;
    }

    if (showMenu) {
      return (
        <AppButton flex={1.5} onPress={AppNavigation.openMenu} transparent>
          <AppIcon flip name="menu" type="custom" size={8} color="#fff" />
        </AppButton>
      );
    }
    return (
      <AppButton transparent flex onPress={goBack}>
        <AppIcon
          flip
          name="ios-arrow-back"
          type="ion"
          size={10}
          color={transparent ? '#000' : '#fff'}
        />
      </AppButton>
    );
  };

  const renderCenter = () => {
    return (
      <AppView flex={4}>
        {logo ? (
          <AppIcon name="logo" type="custom" size={17} />
        ) : (
          <AppText size={bold ? 9 : 8} bold numberOfLines={1} color="#fff">
            {title}
          </AppText>
        )}
      </AppView>
    );
  };
  return (
    <AppView
      center
      stretch
      style={{
        height: APPBAR_HEIGHT,
      }}
      row
      spaceBetween
      backgroundColor={transparent ? 'transparent' : 'primary'}>
      {renderLeft()}
      {renderCenter()}
      {renderRight()}
    </AppView>
  );
};

Header.defaultProps = {
  hideBack: false,
};
export default Header;
