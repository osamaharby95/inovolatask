import React from 'react';
import { AppImage } from '../common';

const Swiper = ({
  SwiperComponent,
  resizeMode,
  data,
  width,
  height,
  fromApi,
}) => {

  return (
      <SwiperComponent
        {...{ width }}
        {...{ height }}
        autoplay
        backgroundColor="#FAFAF9"
        stretch
        center
        >
        {data.map((item, index) => {
          return (
            <AppImage
              key={item + index}
              source={fromApi ? { uri: item } : item}
              {...{ resizeMode }}
              width={100}
              {...height}
              flex
            />
          );
        })}
      </SwiperComponent>
  );
};
Swiper.defaultProps = {
  height: 40,
  width: 100,
  resizeMode: 'cover',
};

export default Swiper;
