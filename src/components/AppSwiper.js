import React from 'react';
import {connect} from 'react-redux';
import Swiper from 'react-native-swiper';
import {
  AppView,
  responsiveWidth,
  moderateScale,
  responsiveHeight,
  AppText,
} from '../common';
import colors from '../common/defaults/colors';

class AppSwiper extends React.Component {
  cricleRad = 2.5;
  renderSwiper = ({
    style,
    dotStyle,
    activeDotStyle,
    children,
    showsPagination,
    marginBottom,
    autoplay,
    index,
  }) => (
    <AppView stretch>
      <Swiper
        width={responsiveWidth(100)}
        Style={[style]}
        {...{autoplay}}
        {...{showsPagination}}
        {...{index}}
        // autoplayTimeout={1}
        dotStyle={[
          {
            width: responsiveWidth(this.cricleRad),
            height: responsiveWidth(this.cricleRad),
            borderRadius: responsiveWidth(this.cricleRad / 2),
            borderWidth: responsiveWidth(0.3),
            borderColor: 'primary',
            marginBottom,
            backgroundColor:"red"
          },
          dotStyle,
        ]}
        activeDotStyle={[
          {
            width: responsiveWidth(this.cricleRad),
            height: responsiveWidth(this.cricleRad),
            borderRadius: responsiveWidth(this.cricleRad / 2),
            marginBottom,
            backgroundColor:"green"
          },
          activeDotStyle,
        ]}
        dotColor="rgba(0,0,0,0)"
        activeDotColor="white"
        // containerStyle={{
        //   transform: [{scaleX: -1}],
        // }}
        horizontal>
        {children}
      </Swiper>
    </AppView>
  );

  render() {
    const {
      containerStyle,
      style,
      dotStyle,
      activeDotStyle,
      showsPagination,
      autoplay,
      children,
      marginBottom,
      index,
      ...rest
    } = this.props;
    return (
      <AppView stretch center {...rest}>
        {this.renderSwiper({
          style,
          dotStyle,
          activeDotStyle,
          children,
          showsPagination,
          marginBottom,
          autoplay,
          index,
        })}
      </AppView>
    );
  }
}
export default connect()(AppSwiper);
