// import AsyncStorage from '@react-native-community/async-storage';
import {AsyncStorage} from "react-native"

import I18n from 'react-native-i18n';

import {SET_LANG} from './types';
import store from '../store/store';

export const setLang = (lang, rtl) => async dispatch => {
  if (rtl === store.getState().lang.rtl) {
    return;
  }
  I18n.locale = lang;
  dispatch({type: SET_LANG, lang, rtl});
  await AsyncStorage.setItem('lang', JSON.stringify({lang, rtl}));
};

export const initLang = (lang, rtl) => async (dispatch, store) => {
  // TODO UNCOMMENT IT
  // await setLang("ar", true)(dispatch, store);

  try {
    const l = await AsyncStorage.getItem('lang');
    if (l) {
      const d = JSON.parse(l);
      await setLang(d.lang, d.rtl)(dispatch, store);
    } else {
      await setLang(lang, rtl)(dispatch, store);
    }
  } catch (error) {}
};
