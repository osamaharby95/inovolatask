import CourseApi from '../api/Course';
import {showError} from '../common';
import I18n from 'react-native-i18n';
import {ApiErrorTypes} from '../api/utils/errors';
export default class Course {
  constructor() {
    this.courseApi = new CourseApi();
  }

  getCourse = async () => {
    let data = [];
    try {
      data = await this.courseApi.getCourse();
    } catch (apiErrorException) {
      if (apiErrorException.type === ApiErrorTypes.CONNECTION_ERROR) {
        showError(I18n.t(apiErrorException.msg));
      } else {
        showError(apiErrorException.msg);
      }
    } finally {
      return data;
    }
  };
}
