import {AppNavigation} from '../common';
import Home from './home/Home';
// register all screens of the app
export default () => {
  AppNavigation.registerScreen('home', Home);
};
