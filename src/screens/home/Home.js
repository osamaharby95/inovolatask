import React, { useEffect, useState } from 'react';
import { StatusBar } from "react-native";
import { ScrollableContainer } from '../../components';
import { AppImage, AppView, AppSpinner, AppText, AppIcon, AppScrollView, AppButton, showSuccess } from '../../common';
import I18n from 'react-native-i18n';
import { useSelector } from 'react-redux';
import { CourseRepo } from '../../repo';
import colors from '../../common/defaults/colors';
import CourseSwiper from '../../components/home/CourseSwiper';
import CourseInfo from '../../components/home/CourseInfo';
import TrainerInfo from '../../components/home/TrainerInfo';
import AboutCourse from '../../components/home/AboutCourse';
import CourseCost from '../../components/home/CourseCost';
const courseRepo = new CourseRepo()
const Home = () => {
  const rtl = useSelector(state => state.lang.rtl);
  const [content, setContent] = useState({
    isLoading: true,
    data: null
  })
  const [isConfirmLoading, setonfirmLoading] = useState(false)

  const getCourseDetails = async () => {
    const res = await courseRepo.getCourse()

    setContent({
      isLoading: false,
      data: res
    })
  }
  useEffect(() => {
    getCourseDetails()
  }, []);
  return (
    <>
      {content.isLoading ?
        <AppView flex stretch center>
          <AppSpinner size={12} />
        </AppView>
        :
        <AppView flex stretch >
          <CourseSwiper data={content.data} />
          <AppScrollView flex stretch >
            <CourseInfo data={content.data} />
            <TrainerInfo data={content.data} />
            <AboutCourse data={content.data} />
            <CourseCost data={content.data} />
          </AppScrollView>
          <AppButton
            stretch
            height={7}
            onPress={() => {
              setonfirmLoading(true)
              setTimeout(() => {
                showSuccess("تم الحجز بنجاح")
                setonfirmLoading(false)
              }, 500);
            }}
          >{isConfirmLoading ?
            <AppSpinner color="#fff" />
            :
            <AppText color="#fff" center>
              قم بالحجز الآن
        </AppText>
            }
          </AppButton>

        </AppView>
      }
    </>
  );
};

export default Home;
