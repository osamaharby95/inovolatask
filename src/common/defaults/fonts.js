export default {
  // normal: 'Tajawal-Regular',
  // bold: 'Tajawal-Bold',
  normal: 'Hacen-Maghreb-Lt',
  bold: 'Hacen-Maghreb-Bd',
  boldIsStyle: true,
};
