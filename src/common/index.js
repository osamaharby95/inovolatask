import './Base/polyfill';

export {default as AppView} from './View';
export {default as AppScrollView} from './ScrollView';
export {default as AppText} from './Text';
export {default as AppIcon} from './Icon';
export {default as AppSpinner} from './Indicator';
export {default as AppButton} from './Button';
export {default as AppImage} from './Image';
export {default as AppNavigation} from './Navigation';
export {default as AppcreateStackNavigation} from './StackNavigation';
export {registerCustomIconType} from './utils/icon';
export {getColors, getColor, getTheme, getFonts} from './Theme';
export {showInfo, showSuccess, showError} from './utils/localNotifications';

export {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
  moderateScale,
  windowWidth,
  windowHeight,
  screenWidth,
  screenHeight,
  statusBarHeight,
} from './utils/responsiveDimensions';
export {default as LocaleEn} from './defaults/en.json';
export {default as LocaleAr} from './defaults/ar.json';
export {default as AppSwiper} from './Swiper';
export {default as TouchableView} from './TouchableView';
