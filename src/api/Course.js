import axios from 'axios';
import {ApiErrorException, ApiErrorTypes} from './utils/errors';
export default class Course {
  getCourse= async () => {
    try {
      const res = await axios.get('https://run.mocky.io/v3/3a1ec9ff-6a95-43cf-8be7-f5daa2122a34');
      console.log('courses', res);
      return res.data;
    } catch (error) {
      console.log(error.response, 'courses error');
      if (!error.response) {
        throw new ApiErrorException(
          ApiErrorTypes.CONNECTION_ERROR,
          'ui-networkConnectionError',
        );
      } else {
        throw new ApiErrorException(
          ApiErrorTypes.GENERAL_ERROR,
          error.response.data.error,
        );
      }
    }
  };
}
