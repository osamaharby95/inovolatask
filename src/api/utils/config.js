import axios from 'axios';
import * as urls from './urls';
import store from '../../store/store';
import {AppNavigation} from '../../common'
export default userData => {
  // Add a request interceptor
  axios.interceptors.request.use(
    config => {
      config.baseURL = urls.BASE_URL;

      return {
        ...config,
        headers: {
          ...config.headers,
          Authorization: config.headers.Authorization,
        },
      };
    },
    error => {
      console.log('conferr', error);

      return Promise.reject(error);
    },
  
  );
};
